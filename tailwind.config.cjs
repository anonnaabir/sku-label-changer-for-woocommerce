/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/**/*.ts',
    './src/**/*.vue',
    './templates/**/*.html',
    './parts/**/*.html',
    ],
  theme: {
    extend: {},
  },
  plugins: [],
}
