=== SKU Label Changer For WooCommerce ===
Contributors: codember, asadabir
Tags: woocommerce,sku,sku to isbn,product sku,mpn,upc
Donate link: https://codember.com/
Requires at least: 3.0.1
Tested up to: 5.6
Requires PHP: 5.4
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A Simple Plugin to change WooCommerce SKU Label to ISBN or any other custom text. No need to touch any code or core WooCommerce functionctions.

== Description ==

What's New: SKU Laber Changer Pro Version Released. [GET PRO HERE](https://codember.com/sku-label-changer-pro-for-woocommerce/)

SKU Label Changer For WooCommerce is a simple but effective plugin for WooCommerce users. Most of the users wants to change the default SKU label. But, unfortunately they don't understand the codes. Also, hiring a web developer is not a good idea for change the simple text.

This plugin allows to to change the text easily. Here are the steps that you need to done.

- Install the plugin from WordPress.org Repository.
- Go to 'General' under settings page
- You will find a textbox named "WooCommerce SKU Label"
- Enter your desired text (Ex:ISBN) and hit the save button.
- Done

== Frequently Asked Questions ==

Does the plugin require any special settings?
Ans: No

If I will disable the plugin, what will happen?
Ans: It will revert to default 'SKU' from your custom text.

My SKU is not changed after using this plugin. What can I do now?
Ans: Just contact our support and we will resolve your issues immedieatly.

== Screenshots ==
1. Settings Page
2. WooCommerce Backend
3. WooCommerce Frontend


## What's new in Pro version

- All in free version
- Add MPN as Product Code
- Add UPC as Product Code
- Multiple Product Code at a time (SKU, MPN, UPC)
- Custom Product Code Filtering
- Visibility Control for Frontend and Admin End

[GET SKU LABEL CHANGER PRO VERSION HERE](https://codember.com/sku-label-changer-pro-for-woocommerce/)

## Privacy Policy
SKU Label Changer For WooCommerce uses [Appsero](https://appsero.com) SDK to collect some telemetry data upon user's confirmation. This helps us to troubleshoot problems faster & make product improvements.

Appsero SDK **does not gather any data by default.** The SDK only starts gathering basic telemetry data **when a user allows it via the admin notice**. We collect the data to ensure a great user experience for all our users.

Integrating Appsero SDK **DOES NOT IMMEDIATELY** start gathering data, **without confirmation from users in any case.**

Learn more about how [Appsero collects and uses this data](https://appsero.com/privacy-policy/).

== Changelog ==

1.0: Initial Release

1.1: README Updated And Asset File Changed

1.2: Few Bug Fixed

1.3: Compatible With Latest WooCommerce Version 3.9.0

1.4: Compatible With Latest WooCommerce Version 4.2.0

1.5:
- Tested on latest WordPress version 5.5
- Compatible With Latest WooCommerce Version 4.5.2
- Plugin Header Updated

1.6:
- Tested on latest WordPress version 5.5.3
- Compatible With Latest WooCommerce Version 4.6.1
- Assets Updated
- Appsero Intregated


1.7:
- Tested on latest WordPress version 5.6
- Compatible With Latest WooCommerce Version 4.9.1
- Admin notice added
- Action Links Added
- Bug Fixed


1.8:
- Tested on latest WordPress version 5.6.1
- Compatible With Latest WooCommerce Version 5.0
- Admin notice bug fixed. User can easily skip SKU Label Pro Notice
- Additional Bug Fixed


1.9:
- Tested on latest WordPress version 5.6.2
- Compatible and Tested With Latest Elementor Version 3.1.1

2.0:
- Bug Fixed
- README and FAQ Updated

2.1:
- Tested on latest WordPress version 5.7
- Compatible With Latest WooCommerce Version 5.1.0
- Undefined Index Bug Fixed


2.2:
- Tested on latest WordPress version 5.7.1
- Compatible With Latest WooCommerce Version 5.2.2


3.0:
- Tested on latest WordPress version 6.0.2
- Compatible With Latest WooCommerce Version 7.0.0
- UI Updated
- Assets Updated
- Appsero Updated
- Added REST API Feature
- README Updated
